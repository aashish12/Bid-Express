<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace'=>'Api'], function(){
    //route to login a user and register a user
    Route::post('/user/login', ['uses'=>'loginController@userLogin', 'as'=>'login.user']);
    Route::post('/user/register', ['uses'=>'logincontroller@userRegister', 'as'=>'register.user']);

    //middleware to autheticate users using passport
    Route::group(['middleware'=>'auth:api'], function(){
        Route::get('/list/items', ['uses'=>'ItemsController@listItemsForBid', 'as'=>'list.items']);

    });
});