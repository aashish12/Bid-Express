<?php

namespace App\Http\Controllers\Api;

use App\BiddersInfo;
use App\Category;
use App\Item;
use App\ItemsForBid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ItemsController extends Controller
{
    //items to be shown at the front page
    public function listItemsForBid()
    {
        $items = ItemsForBid::all();//get all the items that are for bid
        $data = null;//set the default data for data to be sent
        $i = 0;
        //if the item list is empty
        if(count($items)!=0) {
            //loop to get all the items for the items in bid
            foreach ($items as $item) {
                //Only if the item is available
                if($item->availability===null) {
                    $item_info = Item::where('id', $item->item_id)->get();//get the item info for item in bid
                    $data[$i] = [
                        'item' => $item,
                        'item_info' => $item_info,
                        'category' => Category::where('id', $item_info->category_id)
                    ];
                    $i++;
                }
            }
        }
        //send data with the list of items or null with success status
        return response()->json(['data'=>$data, 'message'=>'Items for listing'], 200);
    }

    public function showRecommendedItems(){

    }

    //Add the items for Bid
    public function AddItemForBid(Request $request){//$userId
        $itemName = $request->input('name');
        $description = $request->input('description');
        $category = $request->input('category');
        $min_cost = $request->input('cost');
        $owner = $request->input('userId');
        $expiry = $request->input('expiry');

        //remember to change into files
        $image = $request->input('image');
        DB::beginTransaction();

        try {
            //Adding to the table Item
            $itemInfo = Item::create([
                'name'=>$itemName,
                'item_description'=>$description,
                'category_id'=>$category,
                'image'=>$image,
                'min_cost'=>$min_cost
            ]);

            //Setting item for bid
            ItemsForBid::create([
                'owner_id'=>$owner,
                'item_id'=>$itemInfo->id
            ]);


            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            //returning response with error message with Internal Server Error - 500
            return response()->json(['message'=>'Error on Item Insertion'], 500);
        }
        //returning response with success message with success status
        return response()->json(['message'=>'Successfully Added Item'], 200);

    }

    public function DeleteItem($itemId){
        DB::beginTransction();
        try{
            ItemsForBid::where('item_id', $itemId)->delete();
            BiddersInfo::where('item_id', $itemId)->delete();
            Item::where('id', $itemId)->delete();
            DB::commit();
        }
        catch(\Exception $e)
        {
            DB::rollback();
            //returning response with error message with Internal Server Error - 500
            return response()->json(['message'=>'Error on Deleting an Item'], 500);

        }
        //returning response with success message with status as ok - 200
        return response()->json(['message'=>'Delete Successful'], 200);
    }

}
