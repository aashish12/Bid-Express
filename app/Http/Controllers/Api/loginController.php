<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Userdetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Laravel\Socialite\Facades\Socialite;
use Auth;
use DB;

class loginController extends Controller
{
    //
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        //add route here
        return redirect();
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }

    public function userLogin(Request $request){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken($user->id)->accessToken;
            return response()->json(['success' => $success], 202);
        }
        else{
            return response()->json(['error'=>'Unauthorised'.\request('password')], 401);
        }
    }

    public function userRegister(Request $request){
        //Server side Validation
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required',
            'contact'=>'required',
            'address'=>'required',
        ]);
        if($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        //user data from the form
        $name = $request->input('name');
        $email = $request->input('email');
        $password = bcrypt($request->input('password'));
        $contact = $request->input('contact');
        $address = $request->input('address');
        //Create a user
        DB::beginTransaction();

        try {
            $user = User::create([
                'name' => $name,
                'password' => $password,
                'email' => $email,
                'contact_info' => $contact,
                'address' => $address,
                'role' => 1
            ]);
            Userdetail::insert([
                'user_id'=>$user->id
            ]);
            //generating a token for the user
            $success['token'] = $user->createToken($user->id)->accessToken;
            $success['name'] = $user->name;
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error on creating a user', 'error'=>$e], 500);
        }


        return response()->json(['success'=>$success], 200);
    }

}
