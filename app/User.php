<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    protected $table = 'users';

    public $timestamps = false;

    protected $fillable = array('name', 'contact_info', 'email', 'address', 'role' ,'password');

    protected $hidden = array('password');

    public function biddersInfo()
    {
        return $this->hasMany('App\BiddersInfo', 'bidder_id', 'id');
    }

    public function itemsForBid()
    {
        return $this->hasMany('App\ItemsForBid', 'owner_id', 'id');
    }

    public function userdetail()
    {
        return $this->hasMany('App\Userdetail', 'user_id', 'id');
    }
}
