# SWOT Ananlysis

## Strength

- New to the nepali community
- easy way to get a proper oppurtunity for biding an item
- easy way to get any goods or may be even work done
- on business perspective, an easy way to sell or buy goods
- customers can be buys or sellers
- public/private views over bids

## Weakness

- native language(nepali) platform is lacking
- Security measures
- Interface designs may be dissatisfactory to people
- Product management issues
- currency management
- users and product validation

## Oppurtunities

- evolving technology and user experiences
- world wide links/competitions
- larger datasets about user and goods/items

## Threats

- new websites that may come in the future
- other websites may steal our ideas
- hacking for user data
- improper use of the site for illegal works
