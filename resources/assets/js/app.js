
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

// Vue.component('register', require('./components/register.vue'));

import navbar from './components/navbar.vue'
import example from './components/Example.vue'

const routes = [
    {path: '/example', component: example, name:'example'},
    {path:'/', component: navbar, name:'home'},
    ];

const router = new VueRouter({ routes });

const app = new Vue({
    router }).$mount('#app');