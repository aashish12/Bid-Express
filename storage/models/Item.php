<?php namespace App;

/**
 * Eloquent class to describe the item table
 *
 * automatically generated by ModelGenerator.php
 */
class Item extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'item';

    public $timestamps = false;

    protected $fillable = array('name', 'item_description', 'image', 'min_cost');

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function biddersInfo()
    {
        return $this->hasMany('App\BiddersInfo', 'item_id', 'id');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment', 'item_id', 'id');
    }

    public function droptime()
    {
        return $this->hasMany('App\Droptime', 'item_id', 'id');
    }

    public function itemsForBid()
    {
        return $this->hasMany('App\ItemsForBid', 'item_id', 'id');
    }
}
